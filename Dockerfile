FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# renovate: datasource=repology depName=alpine_3_21/apache2 depType=dependencies versioning=loose
ARG APACHE2_VERSION="2.4.62-r0"

RUN delgroup www-data \
    && addgroup -g 33 www-data \
    && apk --no-cache add \
        apache2="${APACHE2_VERSION}" \
        apache2-proxy="${APACHE2_VERSION}" \
        apache2-ssl="${APACHE2_VERSION}" \
        iproute2~=6 \
        inotify-tools~=4 \
        supervisor~=4 \
        bash~=5 \
        logrotate~=3 \
        run-parts~=4 \
        moreutils~=0 \
        shadow~=4 \
        diffutils~=3 \
        gettext-envsubst~=0

COPY files/ /

RUN chmod go-w /etc/logrotate.d/craynic \
    && rm /var/www/logs /var/www/modules /var/www/run \
    && rm -rf /var/www/localhost \
    && sed -i "s/\(LoadModule\) \([[:alpha:]_]\+\) modules\/\([[:alpha:]_\/.]\+\)$/\1 \2 \/usr\/lib\/apache2\/\3/g" \
      /etc/apache2/httpd.conf /etc/apache2/conf.d/*.conf

WORKDIR /var/www

EXPOSE 80 443

ENV APP_SSL_CERTS_DIR="/etc/ssl/apache2" \
    APP_ENABLE_PROXY_PROTOCOL="no"

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]

CMD ["/usr/local/sbin/supervisord.sh"]
