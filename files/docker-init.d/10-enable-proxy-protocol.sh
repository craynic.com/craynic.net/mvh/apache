#!/usr/bin/env bash

set -Eeuo pipefail

if [[ "$APP_ENABLE_PROXY_PROTOCOL" != "yes" ]]; then
  exit 0
fi

echo "RemoteIPProxyProtocol on" >> /etc/apache2/conf.d/craynic-proxy-protocol.conf
