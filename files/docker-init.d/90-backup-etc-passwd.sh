#!/usr/bin/env bash

set -Eeuo pipefail

ETCGROUP_ORIG="/etc/group.craynic"

getent group > "$ETCGROUP_ORIG"

echo "Backed up groups to $ETCGROUP_ORIG."
