#!/usr/bin/env bash

set -Eeuo pipefail

APACHE_CONFDIR="/etc/apache2"
SITES_AVAIL_DIR="${APACHE_CONFDIR}/sites.d/"

# reload Apache immediately after start
# - this will check whether any sites are defined and if so, it will start Apache
/usr/local/sbin/reload-apache.sh

inotifywait-dir.sh "$SITES_AVAIL_DIR" | while read -r line; do
  echo "$line"

  /usr/local/sbin/reload-apache.sh
done
