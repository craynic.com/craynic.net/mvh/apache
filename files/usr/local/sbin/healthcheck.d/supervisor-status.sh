#!/usr/bin/env bash

set -Eeuo pipefail

# check supervisor status
if [[ -x $(which supervisorctl) ]]; then
	NUM_NOT_RUNNING=$(supervisorctl status | grep -cv "RUNNING\|EXITED\|STOPPED" || true)

	[[ ${NUM_NOT_RUNNING} == 0 ]] || { echo "Supervisor check failed" >&2; exit 1; }
fi

# explicit exit code
exit 0
