#!/usr/bin/env bash

set -Eeuo pipefail

# if Apache2 is installed
if [[ -x $(which httpd) ]]; then
  # if the sites.d directory is ready
  if /usr/local/sbin/sites-directory-status.sh 2>/dev/null; then
    # but Apache2 is not running
    if [[ -x $(which supervisorctl) ]] && ! supervisorctl status apache2 >/dev/null; then
      echo "Sites directory is ready and not empty, but Apache2 is not running" >&2
      exit 1
    fi
  fi
fi

# explicit exit code
exit 0