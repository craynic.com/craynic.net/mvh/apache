#!/usr/bin/env bash

set -Eeuo pipefail

# run supervisord
/usr/bin/supervisord -n -c /etc/supervisord.conf