#!/usr/bin/env bash

set -Eeuo pipefail

if ! /usr/local/sbin/sites-directory-status.sh; then
  if supervisorctl status apache2 >/dev/null; then
    echo "Gracefully quitting Apache."
    supervisorctl stop apache2
  fi
else
  # reload Apache
  if supervisorctl status apache2 >/dev/null; then
    supervisorctl signal USR1 apache2
  else
    supervisorctl start apache2
  fi

  /usr/local/sbin/update-probe.sh
fi
