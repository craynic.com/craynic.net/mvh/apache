#!/usr/bin/env bash

set -Eeuo pipefail

SITES_DIR="/etc/apache2/sites.d/"

if [[ ! -f "$SITES_DIR/.ready" ]]; then
  echo "Sites.d directory $SITES_DIR is not ready" >&2
  exit 1
elif ! compgen -G "$SITES_DIR/*.conf" >/dev/null; then
  echo "Sites.d directory $SITES_DIR is empty" >&2
  exit 2
fi

# explicit exit code
exit 0
