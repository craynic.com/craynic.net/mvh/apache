#!/usr/bin/env bash

set -Eeuo pipefail

inotifywait-dir.sh "$APP_SSL_CERTS_DIR" | while read -r line; do
  echo "$line"

  /usr/local/sbin/reload-apache.sh
done
